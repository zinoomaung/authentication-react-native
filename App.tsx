import { GlobalProvider } from "./src/provider/GlobalProvider";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { ToastProvider } from "react-native-toast-notifications";
import AuthNavigator from "./src/routes/AuthNavigators";
import React from "react";

const App = () => {
  return (
    <GlobalProvider>
      <SafeAreaProvider>
        <ToastProvider>
          <AuthNavigator />
        </ToastProvider>
      </SafeAreaProvider>
    </GlobalProvider>
  );
};

export default App;
