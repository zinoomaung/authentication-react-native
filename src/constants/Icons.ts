export const TabBarIcons: any = {
  home: "apps",
};

export const Icons: any = {
  add: "add-outline",
  back: "arrow-left",
  cashSync: "cash",
  close: "window-close",
  currency: "currency-usd",
  default: "apple",
  delete: "trash-outline",
  download: "download",
  favourite: "heart",
  login: "log-in-outline",
  logout: "logout-variant",
  pokeball: "pokeball",
  profile: "account",
  profileEdit: "account-edit",
  qrcode: "qrcode",
  register: "chevron-forward-circle-outline",
  setting: "cogs",
  share: "share",
  unfavourite: "heart-outline",
};

export const POKEMON_TYPE_ICONS: any = {
  Colorless: "circle-notch",
  Darkness: "moon",
  Dragon: "dragon",
  Fairy: "khanda",
  Fighting: "hand-rock",
  Fire: "fire-alt",
  Grass: "leaf",
  Lightning: "bolt",
  Metal: "meteor",
  Psychic: "khanda",
  Typeless: "question-circle",
  Water: "tint",
};
