import { Labels } from "../constants/Labels";
import { PacmanIndicator } from "react-native-indicators";
import { View, Text, StyleSheet, Modal } from "react-native";
import Colors from "../constants/Colors";
import React from "react";

const Spinner = (props: any) => {
  const loadingMessage = props.loadingMessage ?? Labels.defaultLoading;

  return (
    <>
      <Modal
        animationType="none"
        statusBarTranslucent={true}
        supportedOrientations={["landscape", "portrait"]}
        transparent
        visible={true}
      >
        <View style={styles.container}>
          <View style={styles.background}>
            <PacmanIndicator color={Colors.white} />
            <View style={styles.textContainer}>
              <Text style={styles.textContent}>{loadingMessage}</Text>
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.spinnerOverlay,
    bottom: 0,
    flex: 1,
    left: 0,
    position: "absolute",
    right: 0,
    top: 0,
  },
  background: {
    alignItems: "center",
    bottom: 0,
    justifyContent: "center",
    left: 0,
    position: "absolute",
    right: 0,
    top: 0,
  },
  textContainer: {
    alignItems: "center",
    bottom: 0,
    flex: 1,
    justifyContent: "center",
    left: 0,
    position: "absolute",
    right: 0,
    top: 0,
  },
  textContent: {
    color: Colors.white,
    fontSize: 18,
    fontWeight: "bold",
    top: 40,
  },
});

export default Spinner;
