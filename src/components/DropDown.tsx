import { Dropdown } from "react-native-element-dropdown";
import { Labels } from "../constants/Labels";
import { POKEMON_TYPE_ICONS } from "../constants/Icons";
import { StyleSheet, View, Text } from "react-native";
import Colors from "../constants/Colors";
import Icon from "react-native-vector-icons/FontAwesome5";
import React, { useState } from "react";

const DropDown = ({ data, handleOnChange, type }) => {
  const [value, setValue] = useState(null);
  const TYPES = ["All", ...data].map((value: any, index: number) => { return { label: value, value: index } });
  const placeholder = {
    1: Labels.placeholderType,
    2: Labels.placeholderRarity,
    3: Labels.placeholderSet,
    4: Labels.placeholderSuperType,
  };

  const prepareOptions = (type: any) => {
    return (
      <>
        <View style={styles.optionContainer}>
          <Text style={styles.optionName}>{type.label}</Text>
          <Icon
            color={Colors.TYPE_COLORS[type.label]}
            name={POKEMON_TYPE_ICONS[type.label]}
            size={15}
            style={styles.icon}
          />
        </View>
      </>
    );
  };

  const handleChange = (type: any) => {
    setValue(type.value);
    handleOnChange(type.label);
  };

  return (
    <>
      <Dropdown
        data={TYPES}
        iconStyle={styles.iconStyle}
        labelField="label"
        onChange={handleChange}
        placeholder={placeholder[type]}
        placeholderStyle={styles.placeholderStyle}
        renderItem={prepareOptions}
        renderRightIcon={() => <></>}
        selectedTextStyle={styles.placeholderStyle}
        style={styles.dropdown}
        value={value}
        valueField="value"
      />
    </>
  );
};

const styles = StyleSheet.create({
  dropdown: {
    backgroundColor: Colors.chip9,
    borderColor: Colors.chip8,
    borderRadius: 20,
    borderWidth: 1.5,
    height: 40,
    marginLeft: 10,
    padding: 10,
    width: "30%",
  },
  icon: {
    marginRight: 5,
  },
  iconStyle: {
    height: 25,
    width: 25,
  },
  optionContainer: {
    alignItems: "center",
    flexDirection: "row",
    padding: 10,
    paddingRight: 5,
  },
  optionName: {
    flex: 1,
    fontSize: 15,
  },
  placeholderStyle: {
    fontSize: 15,
    textAlign: "center",
  },
});

export default DropDown;
