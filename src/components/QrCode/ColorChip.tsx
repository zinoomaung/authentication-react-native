import { StyleSheet, TouchableOpacity, View } from "react-native";
import Colors from "../../constants/Colors";
import React from "react";

const ColorChip = ({ color, handleChangeColor, isActive }: any) => {
  return (
    <>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => handleChangeColor(color)}
        style={[styles.chipContainer, { borderColor: isActive ? color : Colors.background }]}
      >
        <View style={[styles.colorContainer, { backgroundColor: color }]} />
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  chipContainer: {
    borderRadius: 20,
    borderWidth: 1.5,
    marginHorizontal: 5,
    padding: 3,
  },
  colorContainer: {
    borderRadius: 20,
    height: 30,
    width: 30,
  },
});

export default ColorChip;
