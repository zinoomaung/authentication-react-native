import { Image, StyleSheet, Text, View } from "react-native";
import Colors from "../constants/Colors";
import React from "react";
import RenderIf from "./RenderIf";

const AppIconImage = (props: any) => {
  return (
    <>
      <View style={styles.container}>
        <RenderIf isTrue={false}>
          <Image
            source={require("../../assets/icon.png")}
            style={styles.image}
          />
        </RenderIf>
      </View>

      <RenderIf isTrue={props.title}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{props.title}</Text>
        </View>
      </RenderIf>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    height: 120,
    padding: 20,
  },
  image: {
    height: 80,
    width: 80,
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
  },
  titleContainer: {
    margin: 30,
    marginBottom: 20,
  },
});

export default AppIconImage;
