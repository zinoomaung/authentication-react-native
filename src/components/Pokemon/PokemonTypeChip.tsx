import { POKEMON_TYPE_ICONS } from "../../constants/Icons";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../../constants/Colors";
import Icon from "react-native-vector-icons/FontAwesome5";
import React from "react";
import RenderIf from "./../RenderIf";

const PokemonTypeChip = (props: any) => {
  const { label, onPress } = props;

  return (
    <>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={onPress}
        style={[styles.chipContainer, { backgroundColor: Colors.TYPE_COLORS[label] }]}
      >
        <View style={styles.buttonContainer}>
          <Icon color={Colors.white} name={POKEMON_TYPE_ICONS[label]} size={13} />

          {/* <RenderIf isTrue={label}>
            <Text style={styles.label}>{label}</Text>
          </RenderIf> */}
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: "center",
    flexDirection: "row",
  },
  chipContainer: {
    alignItems: "center",
    borderRadius: 30,
    height: 22,
    justifyContent: "center",
    marginLeft: 5,
    width: 22,
  },
  label: {
    color: Colors.white,
    fontSize: 13,
    marginLeft: 5,
  },
});

export default PokemonTypeChip;
