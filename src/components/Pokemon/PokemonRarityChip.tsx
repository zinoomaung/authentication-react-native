import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../../constants/Colors";
import React from "react";
import RenderIf from "./../RenderIf";

const PokemonRarityChip = (props: any) => {
  const { label, onPress } = props;

  return (
    <>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={onPress}
        style={[styles.chipContainer, { backgroundColor: Colors.grey4 }]}
      >
        <View style={styles.buttonContainer}>
          <RenderIf isTrue={label}>
            <Text style={styles.label}>{label}</Text>
          </RenderIf>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 5,
    paddingVertical: 3,
  },
  chipContainer: {
    alignSelf: "flex-start",
    borderRadius: 30,
  },
  label: {
    color: Colors.blue1,
    fontSize: 13,
    marginLeft: 5,
  },
});

export default PokemonRarityChip;
