import { Icons } from "../../constants/Icons";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Labels } from "../../constants/Labels";
import Colors from "../../constants/Colors";
import Helper from "../../utils/Helper";
import IconButton from "../IconButton";
import PokemonRarityChip from "./PokemonRarityChip";
import PokemonTypeChip from "./PokemonTypeChip";
import React from "react";
import RenderIf from "../RenderIf";

const height = Helper.getDevice.height;
const width = Helper.getDevice.width;

const PokemonCard = ({ card, favouriteCard, handleFavourite, handleZoomImage }: any) => {
  const favouriteIcon = favouriteCard.includes(card.id) ? Icons.favourite : Icons.unfavourite;

  return (
    <>
      <View>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => handleZoomImage(card.images.large)}
          style={styles.cardContainer}
        >
          <Image
            source={{ uri: card.images.small }}
            style={styles.cardImage}
          />
        </TouchableOpacity>

        <View style={styles.infoContainer}>
          <View style={styles.titleContainer}>
            <RenderIf isTrue={card.name}>
              <Text style={styles.cardName}>{card.name}</Text>
            </RenderIf>
          </View>

          <View style={styles.superTypeContainer}>
            <RenderIf isTrue={card.supertype}>
              <Text style={styles.superType}>{card.supertype}</Text>
            </RenderIf>
          </View>

          <View style={styles.superTypeContainer}>
            <RenderIf isTrue={card.cardmarket?.prices?.trendPrice}>
              <Text style={styles.price}>${card.cardmarket?.prices?.trendPrice}</Text>
            </RenderIf>
          </View>

          <View style={styles.typeContainer}>
            <RenderIf isTrue={card.types}>
              {card.types?.map((type: any, index: any) => <PokemonTypeChip key={index} label={type} />)}
            </RenderIf>

            <RenderIf isTrue={!card.types}>
              <PokemonTypeChip label={Labels.typeless} />
            </RenderIf>
          </View>

          <RenderIf isTrue={card.rarity}>
            <View style={styles.rarityContainer}>
              <PokemonRarityChip label={card.rarity} />
            </View>
          </RenderIf>

          <View style={styles.actionContainer}>
            <View style={styles.actionIconContainer}>
              <IconButton
                color={Colors.chip8}
                name={favouriteIcon}
                onPress={() => handleFavourite(card.id)}
              />
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  actionContainer: {
    alignSelf: "flex-end",
    flexDirection: "row",
    margin: 10,
    marginRight: 0,
  },
  actionIconContainer: {
    alignItems: "center",
    flexDirection: "row",
  },
  cardContainer: {
    alignItems: "center",
    top: 40,
    zIndex: 1,
  },
  cardImage: {
    borderRadius: 5,
    height: height / 3,
    width: width / 1.9,
  },
  cardName: {
    fontSize: 22,
    fontWeight: "bold",
  },
  infoContainer: {
    backgroundColor: Colors.white,
    borderRadius: 10,
    marginHorizontal: 50,
    paddingTop: 40,
    shadowColor: Colors.black,
    shadowOffset: {
      height: 1,
      width: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
  },
  price: {
    color: Colors.orange1,
    fontSize: 16,
    fontWeight: "bold",
    marginHorizontal: 5,
  },
  rarityContainer: {
    flexDirection: "row",
    justifyContent: "center",
    margin: 10,
    marginBottom: 0,
  },
  superType: {
    fontSize: 16,
  },
  superTypeContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 5,
  },
  titleContainer: {
    flexDirection: "row",
    justifyContent: "center",
    margin: 10,
    marginBottom: 0,
  },
  typeContainer: {
    flexDirection: "row",
    justifyContent: "center",
    margin: 10,
    marginBottom: 0,
  },
});

export default PokemonCard;
