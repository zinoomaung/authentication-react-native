import { Icons } from "../../constants/Icons";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import Colors from "../../constants/Colors";
import Helper from "../../utils/Helper";
import IconButton from "../IconButton";
import Modal from "react-native-modal";
import React from "react";

const height = Helper.getDevice.height;
const width = Helper.getDevice.width;

const PokemonImageModal = ({ handleDismiss, source }: any) => {
  return (
    <>
      <Modal
        isVisible={true}
      >
        <View style={styles.imageContainer}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => handleDismiss()}
          >
            <Image
              source={{ uri: source }}
              style={styles.fullScreenImage}
            />
          </TouchableOpacity>

          <View style={styles.closeContainer}>
            <IconButton
              color={Colors.white}
              name={Icons.close}
              onPress={() => handleDismiss()}
              size={50}
              style={styles.icon}
            />
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  closeContainer: {
    paddingTop: 30,
  },
  fullScreenImage: {
    borderRadius: 10,
    height: height / 1.65,
    width: width / 1.06,
  },
  icon: {
    margin: 0,
  },
  imageContainer: {
    alignItems: 'center',
  },
});

export default PokemonImageModal;
