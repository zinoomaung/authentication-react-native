import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../constants/Colors";
import Helper from "../utils/Helper";
import Modal from "react-native-modal";
import React from "react";

const height = Helper.getDevice.height;
const width = Helper.getDevice.width;

const ConfirmModal = ({ label, handler }: any) => {
  const { handleDismiss, handleConfirm } = handler;
  const { message } = label;
  const confirm = label.confirm ?? "OK";
  const dismiss = label.dismiss ?? "Cancel";
  const title = label.title ?? "Confirmation";

  return (
    <>
      <Modal
        backdropOpacity={0.5}
        isVisible={true}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalTitleContainer}>
            <Text style={styles.modalTitleText}>{title}</Text>
          </View>

          <View style={styles.modalBodyContainer}>
            <Text style={styles.modalBodyText}>{message}</Text>
          </View>

          <View style={styles.modalButtonContainer}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={handleDismiss}
              style={styles.modalDismissButton}
            >
              <Text style={styles.modalButtonText}>{dismiss}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.7}
              onPress={handleConfirm}
              style={styles.modalConfirmButton}
            >
              <Text style={styles.modalButtonText}>{confirm}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  modalBodyContainer: {
    flex: 0.5,
    justifyContent: "center",
  },
  modalBodyText: {
    fontSize: 16,
    paddingHorizontal: 20,
    textAlign: "center",
  },
  modalButtonContainer: {
    flex: 0.3,
    flexDirection: "row",
  },
  modalButtonText: {
    color: Colors.white,
    fontSize: 16,
    fontWeight: "bold",
  },
  modalDismissButton: {
    alignItems: "center",
    backgroundColor: Colors.primary,
    borderBottomLeftRadius: 10,
    borderRightColor: Colors.white,
    borderRightWidth: 0.5,
    flex: 0.5,
    justifyContent: "center",
  },
  modalConfirmButton: {
    alignItems: "center",
    backgroundColor: Colors.primary,
    borderBottomRightRadius: 10,
    flex: 0.5,
    justifyContent: "center",
  },
  modalContainer: {
    backgroundColor: Colors.white,
    borderRadius: 10,
    flex: 1,
    marginHorizontal: width / 12,
    marginVertical: height / 2.8,
    paddingTop: 10,
  },
  modalTitleContainer: {
    borderBottomColor: Colors.grey3,
    borderBottomWidth: 0.5,
    flex: 0.2,
    paddingVertical: 5,
  },
  modalTitleText: {
    fontSize: 18,
    fontWeight: "bold",
    paddingHorizontal: 20,
    textAlign: "center",
  },
});

export default ConfirmModal;
