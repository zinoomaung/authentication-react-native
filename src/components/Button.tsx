import { StyleSheet, Text, TouchableOpacity } from "react-native";
import Colors from "../constants/Colors";
import Ionicons from "react-native-vector-icons/Ionicons";
import React from "react";
import RenderIf from "./RenderIf";

const Button = (props: any) => {
  return (
    <>
      <TouchableOpacity {...props} style={[styles.button, { backgroundColor: props.color }]}>
        <Text style={[styles.text, { color: props.textColor }]}>{props.text}</Text>

        <RenderIf isTrue={false}>
          <Ionicons
            color={props.textColor}
            name={props.icon}
            style={styles.icon}
          />
        </RenderIf>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    borderRadius: 30,
    flexDirection: "row",
    height: 50,
    justifyContent: "center",
    margin: 10,
    marginTop: 15,
  },
  icon: {
    fontSize: 25,
    marginLeft: 10,
  },
  text: {
    fontSize: 17,
    fontWeight: "bold",
  },
});

export default Button;
