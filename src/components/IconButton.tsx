import { Icons } from "../constants/Icons";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import Colors from "../constants/Colors";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import React from "react";

const IconButton = (props: any) => {
  return (
    <>
      <TouchableOpacity onPress={props.onPress}>
        <View style={props.style ?? styles.buttonContainer}>
          <MaterialCommunityIcons
            color={props.color ?? Colors.black}
            name={props.name ?? Icons.default}
            size={props.size ?? 30}
          />
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    marginRight: 15,
  },
});

export default IconButton;
