import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../constants/Colors";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import React from "react";
import RenderIf from "./RenderIf";

const ChipButton = (props: any) => {
  const {
    backgroundColor,
    color,
    icon,
    label,
    onPress,
  } = props;

  return (
    <>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={onPress}
        style={[styles.chipContainer, { backgroundColor }]}
      >
        <View style={styles.buttonContainer}>
          <RenderIf isTrue={icon}>
            <MaterialCommunityIcons
              color={color ?? Colors.black}
              name={icon}
              style={styles.icon}
            />
          </RenderIf>

          <RenderIf isTrue={label}>
            <Text style={[styles.label, { color }]}>{label}</Text>
          </RenderIf>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: "center",
    flexDirection: "row",
    margin: 5,
  },
  chipContainer: {
    alignSelf: "flex-start",
    borderRadius: 30,
    margin: 10,
    marginBottom: 0,
    marginRight: 0,
  },
  icon: {
    fontSize: 25,
  },
  label: {
    fontSize: 16,
    fontWeight: "600",
    marginHorizontal: 5,
  },
});

export default ChipButton;
