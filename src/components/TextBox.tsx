import { StyleSheet, TextInput } from "react-native";
import Colors from "../constants/Colors";
import React, { useState } from "react";

const TextBox = (props: any) => {
  const [isFocus, setFocus] = useState(false);

  return (
    <>
      <TextInput
        {...props}
        onBlur={() => setFocus(false)}
        onFocus={() => setFocus(true)}
        style={[styles.input, props.style]}
      />
    </>
  );
};

const styles = StyleSheet.create({
  input: {
    borderColor: Colors.grey2,
    borderRadius: 30,
    borderWidth: 1,
    fontSize: 16,
    height: 50,
    margin: 10,
    marginBottom: 5,
    padding: 10,
    shadowColor: Colors.black,
  },
});

export default TextBox;
