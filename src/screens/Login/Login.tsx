import { actionLogin } from "../../api/Api";
import { Icons } from "../../constants/Icons";
import { Labels } from "../../constants/Labels";
import { RouteNames } from "../../routes/Screens";
import { storeAuthUser, storeToken } from "../../utils/Storage";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import AppIconImage from "../../components/AppIconImage";
import Button from "../../components/Button";
import Colors from "../../constants/Colors";
import React, { useState } from "react";
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";
import TextBox from "../../components/TextBox";

const Login = ({ handleLogin }: any) => {
  const navigation = useNavigation<any>();

  const [email, setEmail] = useState("user1@gmail.com");
  const [errorMessage, setErrorMessage] = useState("");
  const [password, setPassword] = useState("P@ssw0rd");
  const [showLoading, setShowLoading] = useState(false);

  const handleLoginPress = async () => {
    setShowLoading(true);
    const response = await actionLogin(email, password);

    if (response.success == false) {
      setErrorMessage(response.errorMessage);
      setShowLoading(false);
      return false;
    }

    await storeToken(response.token);

    await storeAuthUser({
      email: response.email,
      id: response.id,
      name: response.name,
    });

    setShowLoading(false);
    handleLogin();
  };

  const ErrorMessage = () => {
    return (
      <View style={styles.messageContainer}>
        <RenderIf isTrue={errorMessage}>
          <Text style={styles.message}>{errorMessage}</Text>
        </RenderIf>
      </View>
    );
  };

  return (
    <>
      <RenderIf isTrue={showLoading}>
        <Spinner loadingMessage={Labels.authLoading} />
      </RenderIf>

      <AppIconImage title={Labels.loginTitle} />

      <ErrorMessage />

      <View style={styles.formContainer}>
        <TextBox
          autoCapitalize="none"
          autoCorrect={false}
          defaultValue={email}
          keyboardType="email-address"
          onChangeText={(text: any) => setEmail(text)}
          placeholder={Labels.userId}
          textContentType="none"
        />

        <TextBox
          autoCorrect={false}
          defaultValue={password}
          onChangeText={(text: any) => setPassword(text)}
          placeholder={Labels.password}
          secureTextEntry={true}
          textContentType="none"
        />

        <TouchableOpacity onPress={() => navigation.navigate(RouteNames.forgotPassword)}>
          <Text style={styles.forgotPasswordText}>{Labels.forgotPassword}</Text>
        </TouchableOpacity>

        <Button
          color={Colors.primary}
          icon={Icons.login}
          onPress={() => handleLoginPress()}
          text={Labels.login}
          textColor={Colors.white}
        />

        <TouchableOpacity onPress={() => navigation.navigate(RouteNames.register)}>
          <Text style={styles.registerText}>{Labels.registerNow}</Text>
        </TouchableOpacity>
      </View>

      <View>
        <Text style={styles.versionText}>{Labels.version}</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  forgotPasswordText: {
    color: Colors.blue2,
    fontSize: 15,
    margin: 5,
    textAlign: "right",
  },
  formContainer: {
    flex: 1,
    marginHorizontal: 20,
  },
  message: {
    color: Colors.red1,
    fontSize: 16,
  },
  messageContainer: {
    marginHorizontal: 30,
  },
  registerText: {
    color: Colors.blue2,
    fontSize: 16,
    fontWeight: "bold",
    margin: 5,
    textAlign: "center",
  },
  versionText: {
    color: Colors.grey3,
    fontSize: 16,
    fontWeight: "bold",
    margin: 10,
    marginBottom: 15,
    textAlign: "center",
  },
});

export default Login;
