import { actionFavouriteCard } from "../../api/Pokemon";
import { FlatGrid } from 'react-native-super-grid';
import { getFavouriteCardByUser } from "../../api/Pokemon";
import { StyleSheet } from "react-native";
import Helper from "../../utils/Helper";
import pokemon from "pokemontcgsdk";
import PokemonCard from "../../components/Pokemon/PokemonCard";
import PokemonImageModal from "../../components/Pokemon/PokemonImageModal";
import React, { useEffect, useState } from "react";
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";

const width = Helper.getDevice.width;

const PokemonFavourite = ({ navigation }: any) => {
  const [favouriteCard, setFavouriteCard]: any = useState([]);
  const [imageModalSource, setImageModalSource] = useState(null);
  const [imageModalVisible, setImageModalVisible] = useState(false);
  const [pokemons, setPokemons]: any = useState([]);
  const [showLoading, setShowLoading]: any = useState(false);

  useEffect(() => navigation.addListener('focus', prepareFilterData), [navigation]);

  const prepareFilterData = async () => {
    const cards = [];
    const favorites = await getFavouriteCardByUser();
    setFavouriteCard(favorites);

    await Promise.all(favorites.map(async (value: any) => {
      await pokemon.card.where({ q: "id:" + value }).then((card: any) => cards.push(card.data[0]));
    }));

    setPokemons(cards);
  };

  const handleFavourite = async (newCard: any) => {
    const newPokemons = pokemons.filter((card: any) => card.id !== newCard);
    setPokemons(newPokemons);

    await actionFavouriteCard({ cardId: newCard, isFavourite: false });
  };

  const handleZoomImage = async (source: any) => {
    if (source) {
      setImageModalSource(source);
      setImageModalVisible(true);
    } else {
      setImageModalSource(null);
      setImageModalVisible(false);
    }
  };

  return (
    <>
      <RenderIf isTrue={showLoading}>
        <Spinner />
      </RenderIf>

      <RenderIf isTrue={imageModalSource && imageModalVisible}>
        <PokemonImageModal
          handleDismiss={() => handleZoomImage(false)}
          source={imageModalSource}
        />
      </RenderIf>

      <FlatGrid
        data={pokemons}
        itemDimension={width}
        renderItem={({ item }) => (
          <>
            <PokemonCard
              card={item}
              favouriteCard={favouriteCard}
              handleFavourite={handleFavourite}
              handleZoomImage={handleZoomImage}
              key={item.id}
            />
          </>
        )}
        style={styles.gridView}
      />
    </>
  );
};

const styles = StyleSheet.create({
  gridView: {
    flex: 1,
  },
});

export default PokemonFavourite;
