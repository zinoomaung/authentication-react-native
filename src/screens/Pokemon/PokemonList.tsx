import { actionFavouriteCard, getFavouriteCardByUser } from "../../api/Pokemon";
import { Labels } from "../../constants/Labels";
import { ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { useToast } from "react-native-toast-notifications";
import ChipButton from "../../components/ChipButton";
import Colors from "../../constants/Colors";
import DropDown from "../../components/DropDown";
import pokemon from "pokemontcgsdk";
import PokemonCard from "../../components/Pokemon/PokemonCard";
import PokemonImageModal from "../../components/Pokemon/PokemonImageModal";
import React, { useEffect, useState } from "react";
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";
import TextBox from "../../components/TextBox";

var page = 1;
const defaultPokemons = "name:* ";
const pageSize = 10;

const PokemonList = ({ navigation }: any) => {
  const [favouriteCard, setFavouriteCard]: any = useState([]);
  const [imageModalSource, setImageModalSource] = useState(null);
  const [imageModalVisible, setImageModalVisible] = useState(false);
  const [pokemonRarity, setPokemonRarity]: any = useState([]);
  const [pokemons, setPokemons]: any = useState([]);
  const [pokemonSet, setPokemonSet]: any = useState([]);
  const [pokemonSuperType, setPokemonSuperType]: any = useState([]);
  const [pokemonType, setPokemonType]: any = useState([]);
  const [searchName, setSearchName] = useState("");
  const [searchRarity, setSearchRarity] = useState("");
  const [searchSet, setSearchSet] = useState("");
  const [searchSuperType, setSearchSuperType] = useState("");
  const [searchType, setSearchType] = useState("");
  const [showLoading, setShowLoading]: any = useState(false);
  const toast = useToast();

  useEffect(() => { generateQuery(page) }, [searchRarity, searchSet, searchSuperType, searchType]);
  useEffect(() => { prepareFilterData() }, [navigation]);
  useEffect(() => navigation.addListener('focus', prepareFavouriteCards), [navigation]);

  const prepareFilterData = async () => {
    pokemon.type.all().then((type: any) => setPokemonType(type));
    pokemon.supertype.all().then((type: any) => setPokemonSuperType(type));
    pokemon.rarity.all().then((rarity: any) => setPokemonRarity(rarity));
    pokemon.set.all().then((set: any) => {
      setPokemonSet(set.map((item: any) => { return item.name }))
    });

    prepareFavouriteCards();
  };

  const prepareFavouriteCards = async () => {
    const favorites = await getFavouriteCardByUser();
    setFavouriteCard(favorites);
  };

  const getPokemons = (q: any, page: number) => {
    setShowLoading(true);

    pokemon.card.where({ q, pageSize, page }).then(
      (card: any) => {
        if (card.count == 0) {
          toast.show(Labels.noMoreData);
        }

        setPokemons(page == 1 ? card.data : [...pokemons, ...card.data]);
        setShowLoading(false);
      }
    );
  };

  const loadMorePokemons = () => {
    page += 1;
    generateQuery(page);
  };

  const handleNameChange = () => {
    page = 1;
    generateQuery(page);
  };

  const handleTypeChange = (type: string) => {
    page = 1;
    setSearchType(type == "All" ? "" : type);
  };

  const handleSuperTypeChange = (superType: string) => {
    page = 1;
    setSearchSuperType(superType == "All" ? "" : superType);
  };

  const handleRarityChange = (rarity: string) => {
    page = 1;
    setSearchRarity(rarity == "All" ? "" : rarity);
  };

  const handleSetChange = (set: string) => {
    page = 1;
    setSearchSet(set == "All" ? "" : set);
  };

  const generateQuery = (page: number) => {
    var query = "";
    query += searchName ? 'name:"*' + searchName + '*" ' : defaultPokemons;
    query += searchRarity ? 'rarity:"' + searchRarity + '" ' : " ";
    query += searchType ? "types:" + searchType + " " : " ";
    query += searchSuperType ? "supertype:" + searchSuperType + " " : " ";
    query += searchSet ? 'set.name:"' + searchSet + '" ' : " ";

    getPokemons(query, page);
  };

  const handleFavourite = async (newCard: any) => {
    if (favouriteCard.includes(newCard)) {
      const newFavourite = favouriteCard.filter((card: any) => card !== newCard);
      setFavouriteCard(newFavourite);
      await actionFavouriteCard({ cardId: newCard, isFavourite: false });
    } else {
      setFavouriteCard((oldCards: any) => [...oldCards, newCard]);
      await actionFavouriteCard({ cardId: newCard, isFavourite: true });
    }
  };

  const handleZoomImage = async (source: any) => {
    if (source) {
      setImageModalSource(source);
      setImageModalVisible(true);
    } else {
      setImageModalSource(null);
      setImageModalVisible(false);
    }
  };

  return (
    <>
      <RenderIf isTrue={showLoading}>
        <Spinner />
      </RenderIf>

      <RenderIf isTrue={imageModalSource && imageModalVisible}>
        <PokemonImageModal
          handleDismiss={() => handleZoomImage(false)}
          source={imageModalSource}
        />
      </RenderIf>

      <ScrollView>
        <View style={styles.filterContainer}>
          <TextBox
            autoCapitalize="none"
            autoCorrect={false}
            clearButtonMode="always"
            defaultValue={searchName}
            keyboardType="default"
            onChangeText={(text: any) => setSearchName(text)}
            onSubmitEditing={() => handleNameChange()}
            placeholder={Labels.placeholderName}
            style={styles.filterTextBox}
            textAlign={'center'}
            textContentType="none"
          />

          <View style={styles.optionContainer}>
            <DropDown data={pokemonType} handleOnChange={handleTypeChange} type={1} />
            <DropDown data={pokemonRarity} handleOnChange={handleRarityChange} type={2} />
            <DropDown data={pokemonSet} handleOnChange={handleSetChange} type={3} />
          </View>

          <View style={styles.optionContainer}>
            <DropDown data={pokemonSuperType} handleOnChange={handleSuperTypeChange} type={4} />
          </View>
        </View>

        <View>
          {
            pokemons?.map((pokemon: any, index: any) => {
              return (
                <PokemonCard
                  card={pokemon}
                  favouriteCard={favouriteCard}
                  handleFavourite={handleFavourite}
                  handleZoomImage={handleZoomImage}
                  key={index}
                />
              );
            })
          }
        </View>

        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.chipContainer}
        >
          <ChipButton
            backgroundColor={Colors.grey1}
            label={Labels.loadMore}
            onPress={() => loadMorePokemons()}
          />
        </TouchableOpacity>
      </ScrollView >
    </>
  );
};

const styles = StyleSheet.create({
  chipContainer: {
    alignSelf: "center",
    marginBottom: 50,
    marginTop: 20,
  },
  filterContainer: {
  },
  filterTextBox: {
    borderColor: Colors.chip8,
    borderWidth: 1.5,
    marginBottom: 10,
    padding: 15,
  },
  optionContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
});

export default PokemonList;
