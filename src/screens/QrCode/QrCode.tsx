import { Labels } from '../../constants/Labels';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import ColorChip from '../../components/QrCode/ColorChip';
import Colors from '../../constants/Colors';
import QRCode from 'react-native-qrcode-svg';
import React, { useState } from "react";
import TextBox from '../../components/TextBox';

const QrCode = () => {
  const [qrColor, setQrColor]: any = useState(Colors.black);
  const [qrValue, setQrValue]: any = useState();

  return (
    <>
      <ScrollView>
        <View style={styles.qrImageContainer}>
          <Text style={styles.livePreview}>{Labels.livePreview}</Text>

          <QRCode
            color={qrColor}
            quietZone={10}
            size={250}
            value={qrValue ? qrValue : Labels.loginHeaderTitle}
          />
        </View>

        <View style={styles.colorContainer}>
          {
            Object.entries(Colors.QR_COLORS).map(
              ([key, value]) => {
                return (
                  <ColorChip
                    color={value}
                    handleChangeColor={setQrColor}
                    isActive={qrColor == value}
                    key={key}
                  />
                );
              }
            )
          }
        </View>

        <View style={styles.inputContainer}>
          <TextBox
            autoCapitalize="none"
            autoCorrect={false}
            clearButtonMode="always"
            defaultValue={qrValue}
            keyboardType="default"
            onChangeText={(text: any) => setQrValue(text)}
            placeholder={Labels.placeholderQrCode}
            style={styles.qrTextBox}
            textContentType="none"
          />
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  colorContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  inputContainer: {
    justifyContent: 'center',
    marginHorizontal: 30,
    marginTop: 10,
  },
  livePreview: {
    fontSize: 18,
    fontWeight: "600",
    marginBottom: 20,
  },
  qrImageContainer: {
    alignItems: 'center',
    marginVertical: 30,
  },
  qrTextBox: {
    borderColor: Colors.black,
    borderRadius: 5,
    borderWidth: 1.5,
    padding: 15,
  },
});

export default QrCode;
