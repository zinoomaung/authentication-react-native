import React, { useState } from "react";
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";

const Profile = ({ navigation }: any) => {
  const [showLoading, setShowLoading]: any = useState(false);

  return (
    <>
      <RenderIf isTrue={showLoading}>
        <Spinner />
      </RenderIf>
    </>
  );
};

export default Profile;
