import { actionResetPassword } from "../../api/Api";
import { Icons } from "../../constants/Icons";
import { Labels } from "../../constants/Labels";
import { RouteNames } from "../../routes/Screens";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import AppIconImage from "../../components/AppIconImage";
import Button from "../../components/Button";
import Colors from "../../constants/Colors";
import React, { useState } from "react";
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";
import TextBox from "../../components/TextBox";

const ResetPassword = ({ route, navigation }: any) => {
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState(route.params.email);
  const [errorMessage, setErrorMessage] = useState("");
  const [isReset, setIsReset] = useState(false);
  const [password, setPassword] = useState("");
  const [resetPasswordToken, setToken] = useState(route.params.resetPasswordToken);
  const [showLoading, setShowLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");

  const handleResetPress = async () => {
    setShowLoading(true);
    const response = await actionResetPassword({ email, password, confirmPassword, resetPasswordToken });

    if (response.success == false) {
      setErrorMessage(response.errorMessage);
      setSuccessMessage("");
      setIsReset(false);
    } else {
      setErrorMessage("");
      setSuccessMessage(response.message);
      setIsReset(true);
    }
    setShowLoading(false);
  };

  const ErrorMessage = () => {
    return (
      <View style={styles.messageContainer}>
        <RenderIf isTrue={errorMessage}>
          <Text style={styles.message}>{errorMessage}</Text>
        </RenderIf>

        <RenderIf isTrue={successMessage}>
          <Text style={[styles.message, { color: Colors.green1 }]}>{successMessage}</Text>
        </RenderIf>
      </View>
    );
  };

  return (
    <>
      <RenderIf isTrue={showLoading}>
        <Spinner />
      </RenderIf>

      <AppIconImage title={Labels.resetTitle} />

      <ErrorMessage />

      <View style={styles.formContainer}>
        <RenderIf isTrue={!isReset}>
          <TextBox
            autoCorrect={false}
            defaultValue={password}
            onChangeText={(text: any) => setPassword(text)}
            placeholder={Labels.password}
            secureTextEntry={true}
            textContentType="none"
          />

          <TextBox
            autoCorrect={false}
            defaultValue={confirmPassword}
            onChangeText={(text: any) => setConfirmPassword(text)}
            placeholder={Labels.confirmPassword}
            secureTextEntry={true}
            textContentType="none"
          />

          <Button
            color={Colors.primary}
            icon={Icons.register}
            onPress={() => handleResetPress()}
            text={Labels.resetPassword}
            textColor={Colors.white}
          />

          <TouchableOpacity onPress={() => navigation.navigate(RouteNames.login)}>
            <Text style={styles.loginText}>{Labels.goBackTologin}</Text>
          </TouchableOpacity>
        </RenderIf>

        <RenderIf isTrue={isReset}>
          <Button
            color={Colors.primary}
            icon={Icons.register}
            onPress={() => navigation.navigate(RouteNames.login)}
            text={Labels.goBackTologin}
            textColor={Colors.white}
          />
        </RenderIf>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    marginHorizontal: 20,
  },
  loginText: {
    color: Colors.blue2,
    fontSize: 16,
    fontWeight: "bold",
    margin: 5,
    textAlign: "center",
  },
  message: {
    color: Colors.red1,
    fontSize: 16,
  },
  messageContainer: {
    marginHorizontal: 30,
  },
});

export default ResetPassword;
