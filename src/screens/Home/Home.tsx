import { Icons } from "../../constants/Icons";
import { Labels } from "../../constants/Labels";
import { RouteNames } from "../../routes/Screens";
import { ScrollView, StyleSheet } from "react-native";
import ChipButton from "../../components/ChipButton";
import Colors from "../../constants/Colors";
import React, { useState } from "react";
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";

const Home = ({ navigation }: any) => {
  const [showLoading, setShowLoading]: any = useState(false);

  return (
    <>
      <RenderIf isTrue={showLoading}>
        <Spinner />
      </RenderIf>

      <ScrollView>
        <ChipButton
          backgroundColor={Colors.chip2}
          color={Colors.white}
          icon={Icons.cashSync}
          label={Labels.cashSync}
          onPress={() => navigation.navigate(RouteNames.pokemonList)}
        />

        <ChipButton
          backgroundColor={Colors.chip8}
          color={Colors.white}
          icon={Icons.pokeball}
          label={Labels.pokemon}
          onPress={() => navigation.navigate(RouteNames.pokemonList)}
        />

        <ChipButton
          backgroundColor={Colors.chip3}
          color={Colors.white}
          icon={Icons.currency}
          label={Labels.currency}
          onPress={() => navigation.navigate(RouteNames.pokemonList)}
        />

        <ChipButton
          backgroundColor={Colors.black}
          color={Colors.white}
          icon={Icons.qrcode}
          label={Labels.qrCode}
          onPress={() => navigation.navigate(RouteNames.qrCode)}
        />
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
});

export default Home;
