import Home from "../screens/Home/Home";
import PokemonFavourite from "../screens/Pokemon/PokemonFavourite";
import PokemonList from "../screens/Pokemon/PokemonList";
import Profile from "../screens/Profile/Profile";
import QrCode from "../screens/QrCode/QrCode";
import Setting from "../screens/Setting/Setting";

export { Home, PokemonFavourite, PokemonList, Profile, QrCode, Setting };
