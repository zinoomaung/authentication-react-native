import { actionForgotPassword } from "../../api/Api";
import { Icons } from "../../constants/Icons";
import { Labels } from "../../constants/Labels";
import { RouteNames } from "../../routes/Screens";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import AppIconImage from "../../components/AppIconImage";
import Button from "../../components/Button";
import Colors from "../../constants/Colors";
import React, { useState } from "react";
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";
import TextBox from "../../components/TextBox";

const ForgotPassword = ({ navigation }: any) => {
  const [email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [showLoading, setShowLoading] = useState(false);

  const handleForgotPress = async () => {
    setErrorMessage("");
    setShowLoading(true);
    const response = await actionForgotPassword(email);

    setShowLoading(false);
    if (response.success == false) {
      setErrorMessage(response.errorMessage);
    } else {
      navigation.navigate(RouteNames.resetPassword, {
        email,
        resetPasswordToken: response.resetPasswordToken,
      })
    }
  };

  const ErrorMessage = () => {
    return (
      <View style={styles.messageContainer}>
        <RenderIf isTrue={errorMessage}>
          <Text style={styles.message}>{errorMessage}</Text>
        </RenderIf>
      </View>
    );
  };

  return (
    <>
      <RenderIf isTrue={showLoading}>
        <Spinner />
      </RenderIf>

      <AppIconImage title={Labels.forgotTitle} />

      <ErrorMessage />

      <View style={styles.formContainer}>
        <TextBox
          autoCapitalize="none"
          autoCorrect={false}
          defaultValue={email}
          keyboardType="email-address"
          onChangeText={(text: any) => setEmail(text)}
          placeholder={Labels.userId}
          textContentType="none"
        />

        <Button
          color={Colors.primary}
          icon={Icons.register}
          onPress={() => handleForgotPress()}
          text={Labels.send}
          textColor={Colors.white}
        />

        <TouchableOpacity onPress={() => navigation.navigate(RouteNames.login)}>
          <Text style={styles.loginText}>{Labels.goBackTologin}</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    marginHorizontal: 20,
  },
  loginText: {
    color: Colors.blue2,
    fontSize: 16,
    fontWeight: "bold",
    margin: 5,
    textAlign: "center",
  },
  message: {
    color: Colors.red1,
    fontSize: 16,
  },
  messageContainer: {
    marginHorizontal: 30,
  },
});

export default ForgotPassword;
