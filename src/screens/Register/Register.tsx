import { actionRegister } from "../../api/Api";
import { Icons } from "../../constants/Icons";
import { Labels } from "../../constants/Labels";
import { RouteNames } from "../../routes/Screens";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import AppIconImage from "../../components/AppIconImage";
import Button from "../../components/Button";
import Colors from "../../constants/Colors";
import React, { useState } from "react";
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";
import TextBox from "../../components/TextBox";

const Register = () => {
  const navigation = useNavigation<any>();

  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [isRegistered, setIsRegistered] = useState(false);
  const [password, setPassword] = useState("");
  const [showLoading, setShowLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [userName, setUserName] = useState("");

  const handleRegisterPress = async () => {
    setShowLoading(true);
    const data = {
      email,
      name: userName,
      password,
      confirmPassword,
    };

    const response = await actionRegister(data);

    if (response.success == false) {
      setErrorMessage(response.errorMessage);
      setSuccessMessage("");
      setShowLoading(false);
    } else {
      setErrorMessage("");
      setSuccessMessage(response.message);
      setShowLoading(false);
      setIsRegistered(true);
    }
  };

  const ErrorMessage = () => {
    return (
      <View style={styles.messageContainer}>
        <RenderIf isTrue={errorMessage}>
          <Text style={styles.message}>{errorMessage}</Text>
        </RenderIf>

        <RenderIf isTrue={successMessage}>
          <Text style={[styles.message, { color: Colors.green1 }]}>{successMessage}</Text>
        </RenderIf>
      </View>
    );
  };

  return (
    <>
      <RenderIf isTrue={showLoading}>
        <Spinner loadingMessage={Labels.registerLoading} />
      </RenderIf>

      <AppIconImage title={Labels.registerTitle} />

      <ErrorMessage />

      <View style={styles.formContainer}>
        <RenderIf isTrue={!isRegistered}>
          <TextBox
            autoCapitalize="none"
            autoCorrect={false}
            defaultValue={userName}
            keyboardType="default"
            onChangeText={(text: any) => setUserName(text)}
            placeholder={Labels.userName}
            textContentType="none"
          />

          <TextBox
            autoCapitalize="none"
            autoCorrect={false}
            defaultValue={email}
            keyboardType="email-address"
            onChangeText={(text: any) => setEmail(text)}
            placeholder={Labels.userId}
            textContentType="none"
          />

          <TextBox
            autoCorrect={false}
            defaultValue={password}
            onChangeText={(text: any) => setPassword(text)}
            placeholder={Labels.password}
            secureTextEntry={true}
            textContentType="none"
          />

          <TextBox
            autoCorrect={false}
            defaultValue={confirmPassword}
            onChangeText={(text: any) => setConfirmPassword(text)}
            placeholder={Labels.confirmPassword}
            secureTextEntry={true}
            textContentType="none"
          />

          <Button
            color={Colors.primary}
            icon={Icons.register}
            onPress={() => handleRegisterPress()}
            text={Labels.register}
            textColor={Colors.white}
          />

          <TouchableOpacity onPress={() => navigation.navigate(RouteNames.login)}>
            <Text style={styles.loginText}>{Labels.goBackTologin}</Text>
          </TouchableOpacity>
        </RenderIf>

        <RenderIf isTrue={isRegistered}>
          <Button
            color={Colors.primary}
            icon={Icons.register}
            onPress={() => navigation.navigate(RouteNames.login)}
            text={Labels.goBackTologin}
            textColor={Colors.white}
          />
        </RenderIf>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    marginHorizontal: 20,
  },
  loginText: {
    color: Colors.blue2,
    fontSize: 16,
    fontWeight: "bold",
    margin: 5,
    textAlign: "center",
  },
  message: {
    color: Colors.red1,
    fontSize: 16,
  },
  messageContainer: {
    marginHorizontal: 30,
  },
});

export default Register;
