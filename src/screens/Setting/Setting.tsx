import { getCurrentUserInfo } from "../../api/Api";
import { Icons } from "../../constants/Icons";
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from "../../constants/Colors";
import IconButton from "../../components/IconButton";
import React, { useEffect, useState } from "react"
import RenderIf from "../../components/RenderIf";
import Spinner from "../../components/Spinner";

const Setting = ({ navigation }: any) => {
  const [showLoading, setShowLoading]: any = useState(false);
  const [userInfo, setUserInfo] = useState(null);

  useEffect(() => { fetchUserInfo() }, [navigation]);

  const fetchUserInfo = async () => {
    setShowLoading(true);

    const user = await getCurrentUserInfo();
    setUserInfo(user.data);

    setShowLoading(false);
  };

  return (
    <>
      <ScrollView>
        <RenderIf isTrue={showLoading}>
          <Spinner />
        </RenderIf>

        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.userContainer}
        >
          <View style={styles.imageContainer}>
            <Image
              source={require("../../../assets/icon.png")}
              style={styles.image}
            />
          </View>

          <View style={styles.userInfoContainer}>
            <Text style={styles.name}>{userInfo?.name}</Text>
            <Text style={styles.email}>{userInfo?.email}</Text>
          </View>

          <View style={styles.editContainer}>
            <IconButton
              color={Colors.primary}
              name={Icons.profileEdit}
            />
          </View>
        </TouchableOpacity>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  editContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  email: {
    color: Colors.chip5,
    fontSize: 16,
  },
  image: {
    height: 40,
    width: 40,
  },
  imageContainer: {
    justifyContent: 'center',
    margin: 10,
  },
  name: {
    fontSize: 18,
    marginBottom: 5,
  },
  userContainer: {
    backgroundColor: Colors.white,
    borderRadius: 5,
    flexDirection: 'row',
    margin: 10,
    shadowColor: Colors.black,
    shadowOffset: {
      height: 1,
      width: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
  },
  userInfoContainer: {
    justifyContent: 'center',
    margin: 10,
  },
});

export default Setting;