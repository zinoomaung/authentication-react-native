import { Icons, TabBarIcons } from "../constants/Icons";
import { Labels } from "../constants/Labels";
import { RouteNames } from "./Screens";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Colors from "../constants/Colors";
import ConfirmModal from "../components/ConfirmModal";
import Helper from "../utils/Helper";
import IconButton from "../components/IconButton";
import Logger from "../utils/Logger";
import React from "react";
import RenderIf from "../components/RenderIf";

const toShowTabBars: any = [RouteNames.home];

export const headerOptions = ({
  handleLogout,
  navigation,
  route,
}: any): any => {
  const [showLogoutModal, setShowLogoutModal] = useState(false);
  const currentRouteName = route.name;
  const headerBackgrounds = {
    Home: Colors.primary,
    PokemonFavourite: Colors.chip8,
    PokemonList: Colors.chip8,
    QrCode: Colors.black,
    Setting: Colors.primary,
  };
  const hideBackButton = false;

  const goPreviousScreen = () => navigation.goBack();

  const goScreen = (screen: string, param = {}) => navigation.navigate(screen, param);

  const BackButton = () => (
    <TouchableOpacity
      onPress={() => goPreviousScreen()}
      style={styles.backButton}
    >
      <IconButton
        color={Colors.white}
        name={Icons.back}
        onPress={() => goPreviousScreen()}
        style={{ marginLeft: 5 }}
      />
      {/* <RenderIf isTrue={Helper.getPlatform === "ios"}>
        <Text style={styles.backButtonLabel}>Back</Text>
      </RenderIf> */}
    </TouchableOpacity>
  );

  const logoutHandler = {
    handleConfirm: async () => {
      setShowLogoutModal(false);

      await AsyncStorage.clear();
      Logger.info("LOGGED OUT");
      Logger.info("AsyncStorage getAllKeys");
      Logger.info(await AsyncStorage.getAllKeys());
      handleLogout();
    },
    handleDismiss: () => setShowLogoutModal(false),
  };

  const logoutLabel = {
    confirm: "Log Out",
    message: "Do you want to logout?",
  };

  const DefaultHeaderIcon = () => (
    <View style={styles.rightButtons}>
      <RenderIf isTrue={currentRouteName == RouteNames.home}>
        <IconButton
          color={Colors.white}
          name={Icons.logout}
          onPress={() => setShowLogoutModal(true)}
        />

        <IconButton
          color={Colors.white}
          name={Icons.setting}
          onPress={() => goScreen(RouteNames.setting)}
        />
      </RenderIf>

      <RenderIf isTrue={currentRouteName == RouteNames.pokemonList}>
        <IconButton
          color={Colors.white}
          name={Icons.favourite}
          onPress={() => goScreen(RouteNames.pokemonFavourite)}
        />
      </RenderIf>

      <RenderIf isTrue={showLogoutModal}>
        <ConfirmModal handler={logoutHandler} label={logoutLabel} />
      </RenderIf>
    </View>
  );

  const HeaderLeft = () => {
    if (toShowTabBars.includes(currentRouteName)) {
      return <Text style={styles.leftHeaderTitle}>{Labels.loginHeaderTitle}</Text>;
    } else if (hideBackButton) {
      return null;
    } else {
      return <BackButton />;
    }
  };

  const HeaderRight = () => <DefaultHeaderIcon />;

  const HeaderTitle = () => {
    if (toShowTabBars.includes(currentRouteName)) {
      return null;
    } else {
      // return <Text style={styles.headerTitleStyle}>{Labels.loginHeaderTitle}</Text>;
    }
  };

  return {
    headerLeft: HeaderLeft,
    headerRight: HeaderRight,
    headerShadowVisible: false,
    headerStyle: { backgroundColor: headerBackgrounds[currentRouteName] ?? Colors.primary },
    headerTitle: HeaderTitle,
    headerTitleAlign: "center",
  };
};

export const tabBarOptions = ({ route }: any): any => {
  const currentRouteName = route.name;
  const tabBarLabels: any = {
    home: "Home",
  };
  const defaultTabBar = {
    backgroundColor: Colors.primary,
  };
  const hideTabBar = { display: "none" };
  const isHomeScreen = false;
  const tabBarStyle = isHomeScreen ? defaultTabBar : hideTabBar;

  const prepareForTabBarIcon = (metaData: any) => {
    const { focused, color, size } = metaData;
    const routeName = currentRouteName.toLowerCase();
    const iconName = TabBarIcons[routeName];
    return <IconButton color={Colors.white} name={iconName} style={{ margin: 0 }} />;
  };

  const options = {
    tabBarButton: toShowTabBars.includes(currentRouteName)
      ? undefined
      : () => null,
    tabBarIcon: prepareForTabBarIcon,
    tabBarItemStyle: { color: Colors.white },
    tabBarLabel: tabBarLabels[currentRouteName.toLowerCase()],
    tabBarLabelStyle: { color: Colors.white, fontSize: 14, marginBottom: 3 },
    tabBarStyle,
  };

  return options;
};

const styles = StyleSheet.create({
  backButton: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 5,
    width: 70,
  },
  backButtonLabel: {
    color: Colors.white,
    fontSize: 18,
  },
  headerTitleStyle: {
    color: Colors.white,
    fontSize: 18,
    fontWeight: "bold",
  },
  leftHeaderTitle: {
    color: Colors.white,
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 10,
  },
  rightButtons: {
    flexDirection: "row",
  },
});
