import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { getToken } from "../utils/Storage";
import { Labels } from "../constants/Labels";
import { NavigationContainer } from "@react-navigation/native";
import { RouteNames } from "./Screens";
import Colors from "../constants/Colors";
import DashboardNavigators from "./DashboardNavigators";
import ForgotPassword from "../screens/ForgotPassword/ForgotPassword";
import Login from "../screens/Login/Login";
import React, { useEffect, useState } from "react";
import Register from "../screens/Register/Register";
import ResetPassword from "../screens/ResetPassword/ResetPassword";

const { Navigator, Screen } = createNativeStackNavigator();

const AuthNavigator = () => {
  const [isAuth, isSetAuth] = useState(false);
  const login = () => isSetAuth(true);
  const logout = () => isSetAuth(false);

  const LoginScreen = () => <Login handleLogin={login} />;
  const DashBoard = () => <DashboardNavigators handleLogout={logout} />;

  const screenOptions: any = {
    dashboard: { headerShown: false },
    login: {
      headerLeft: () => <></>,
      headerStyle: { backgroundColor: Colors.primary },
      headerTintColor: Colors.white,
      headerTitle: Labels.loginHeaderTitle,
      headerTitleAlign: "center",
    },
  };

  useEffect(() => {
    getToken().then((token) => isSetAuth(token ? true : false));
  }, []);

  return (
    <NavigationContainer>
      <Navigator>
        {isAuth ? (
          <Screen
            component={DashBoard}
            name="DashBoard"
            options={screenOptions.dashboard}
          />
        ) : (
          <>
            <Screen
              component={LoginScreen}
              name={RouteNames.login}
              options={screenOptions.login}
            />
            <Screen
              component={Register}
              name={RouteNames.register}
              options={screenOptions.login}
            />
            <Screen
              component={ForgotPassword}
              name={RouteNames.forgotPassword}
              options={screenOptions.login}
            />
            <Screen
              component={ResetPassword}
              name={RouteNames.resetPassword}
              options={screenOptions.login}
            />
          </>
        )}
      </Navigator>
    </NavigationContainer>
  );
};

export default AuthNavigator;
