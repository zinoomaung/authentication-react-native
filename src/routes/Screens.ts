export const RouteNames = {
  forgotPassword: "ForgotPassword",
  home: "Home",
  login: "Login",
  pokemonFavourite: "PokemonFavourite",
  pokemonList: "PokemonList",
  profile: "Profile",
  qrCode: "QrCode",
  register: "Register",
  resetPassword: "ResetPassword",
  setting: "Setting",
};
