import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { headerOptions, tabBarOptions } from "./NavigatorOptions";
import { RouteNames } from "./Screens";
import * as Screens from "../screens/index";
import React from "react";

const { Navigator, Screen } = createBottomTabNavigator();

const DashboardNavigators = ({ handleLogout }: any) => {
  const options = ({ route, navigation }: any) =>
    headerOptions({ route, navigation, handleLogout });

  return (
    <>
      <Navigator backBehavior={"history"} screenOptions={tabBarOptions}>
        <Screen name={RouteNames.home} component={Screens.Home} options={options} />
        <Screen name={RouteNames.profile} component={Screens.Profile} options={options} />
        <Screen name={RouteNames.pokemonList} component={Screens.PokemonList} options={options} />
        <Screen name={RouteNames.pokemonFavourite} component={Screens.PokemonFavourite} options={options} />
        <Screen name={RouteNames.setting} component={Screens.Setting} options={options} />
        <Screen name={RouteNames.qrCode} component={Screens.QrCode} options={options} />
      </Navigator>
    </>
  );
};

export default DashboardNavigators;
