import AsyncStorage from "@react-native-async-storage/async-storage";

// key
const AUTH_USER = "_AUTH_USER";
const TOKEN = "_TOKEN";
const TOKEN_CREATED_AT = "_TOKEN_CREATED_AT";

// time-to-live in hours
const TTL = 5;

// retrieve
export const getToken = async () => {
  const createdAt: any = await AsyncStorage.getItem(TOKEN_CREATED_AT);

  if (createdAt == null) {
    await AsyncStorage.clear();
    return null;
  }

  const now = new Date().getTime();
  const diffInSeconds = Math.ceil(now - JSON.parse(createdAt));
  const hour = 60 * 60 * 1000;
  const sessionHours = Math.floor(diffInSeconds / hour);

  if (sessionHours > TTL) {
    console.log("session expired");
    await AsyncStorage.clear();
    return null;
  }

  return await AsyncStorage.getItem(TOKEN);
};

export const getAuthUser = async () => {
  const data: any = await AsyncStorage.getItem(AUTH_USER);
  return JSON.parse(data);
};

// store
export const storeToken = async (token: any) => {
  await AsyncStorage.setItem(TOKEN, token);
  await AsyncStorage.setItem(
    TOKEN_CREATED_AT,
    JSON.stringify(new Date().getTime())
  );
};

export const storeAuthUser = async (user: any) => {
  await AsyncStorage.setItem(AUTH_USER, JSON.stringify(user));
};
