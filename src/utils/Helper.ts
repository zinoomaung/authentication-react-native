import { Dimensions, Platform } from "react-native";
import Colors from "../constants/Colors";
import Moment from "moment";
import NetInfo from "@react-native-community/netinfo";

const checkConnectivity = () => {
  return new Promise((resolve, _) => {
    if (Platform.OS === "android") {
      NetInfo.fetch().then((state) => resolve(state.isInternetReachable));
    } else {
      const unsubscribe = () =>
        NetInfo.addEventListener((state) => {
          unsubscribe();
          resolve(state.isInternetReachable);
        });
    }
  });
};

const netInfo = async () => {
  const status = await NetInfo.fetch();
  console.log(status.isConnected);
};

const getCurrentDate = () => {
  return Moment(new Date()).format("yyyy/MM/DD");
};

const getCurrentDateTime = () => {
  return Moment(new Date()).format("yyyy/MM/DD HH:mm");
};

const getDateFromDateTime = (date: any) => {
  return Moment(new Date(date)).format("yyyy/MM/DD");
};

const getDevice = {
  height: Dimensions.get("window").height,
  width: Dimensions.get("window").width,
};

const getTextColor = (hex: any) => {
  const pattern1 = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  const pattern2 = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;
  hex = hex.replace(pattern1, (r: any, g: any, b: any) => {
    return r + r + g + g + b + b;
  });
  const rgb: any = pattern2.exec(hex);

  const r = parseInt(rgb[1], 16);
  const g = parseInt(rgb[2], 16);
  const b = parseInt(rgb[3], 16);

  return (r * 299 + g * 587 + b * 114) / 1000 > 123
    ? Colors.black
    : Colors.white;
};

const getPlatform = Platform.OS;

const getTimeFromDateTime = (date: any) => {
  return Moment(date).format("HH:mm");
};

const groupByKey = (items: any, key: any) => {
  return items.reduce(
    (value: any, item: any) => ({
      ...value,
      [item[key]]: [...(value[item[key]] || []), item],
    }),
    {}
  );
};

const isBeforeToday = (date: any) => {
  return Moment(new Date(date)).isBefore();
};

const Helper = {
  checkConnectivity,
  getCurrentDate,
  getCurrentDateTime,
  getDateFromDateTime,
  getDevice,
  getPlatform,
  getTextColor,
  getTimeFromDateTime,
  groupByKey,
  isBeforeToday,
  netInfo,
};

export default Helper;
