const _GREEN = "\x1b[42m";
const _PINK = "\x1b[45m";
const _RED = "\x1b[41m";
const _RESET = "\x1b[0m";

export const api = (message: any) => {
  const defaultMessage = "++++++++++++++++++++++++++++++";
  console.log(_PINK, defaultMessage, message, _RESET);
};

export const info = (message: any) => {
  const defaultMessage = "-----";
  console.log(defaultMessage + defaultMessage, message);
};

export default {
  api,
  info,
};
