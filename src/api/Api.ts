import { AuthenticationSystem } from "./Routes";
import { getAuthUser, getToken } from "../utils/Storage";
import axios from "axios";
import Logger from "../utils/Logger";

const URLS = { local: "https://authentication-api-zom.onrender.com/" };

export const APP_SERVER_URL = URLS.local;

export const axiosInstance = axios.create({
  baseURL: APP_SERVER_URL,
});

const authHeader: any = {
  headers: { "Content-Type": "application/json" },
};

const postHeader: any = {
  headers: { "Content-Type": "application/json" },
};

export const doAuthorization = async (): Promise<any> => {
  const token: any = await getToken();
  postHeader.headers._token = `Bearer ${token}`;

  return postHeader;
};

export const extractError = (raw: any) => {
  try {
    const error = { ...raw };

    const SERVER_ERROR = ["ERR_BAD_RESPONSE", "ERR_NETWORK"];

    if (SERVER_ERROR.includes(error.code)) {
      return {
        code: error.code,
        errorMessage: error.message,
        success: false,
      };
    }

    const response = error.response.data;
    const code = error.response.status;

    return { code, ...response, success: false };
  } catch (error: any) {
    return {
      code: 500,
      errorMessage: "Server Error.",
      success: false,
    };
  }
};

export const actionLogin = async (
  email: string,
  password: string
): Promise<any> => {
  try {
    Logger.api("actionLogin");
    const response = await axiosInstance.post(
      AuthenticationSystem.login,
      { email, password },
      authHeader
    );

    return response.data;
  } catch (error: any) {
    return extractError(error);
  }
};

export const actionRegister = async (user: any): Promise<any> => {
  try {
    Logger.api("actionRegister");
    const response = await axiosInstance.post(
      AuthenticationSystem.register,
      user,
      authHeader
    );

    return response.data;
  } catch (error: any) {
    return extractError(error);
  }
};

export const actionForgotPassword = async (email: string): Promise<any> => {
  try {
    Logger.api("actionForgotPassword");
    const response = await axiosInstance.post(
      AuthenticationSystem.forgot,
      { email },
      authHeader
    );

    return response.data;
  } catch (error: any) {
    return extractError(error);
  }
};

export const actionResetPassword = async (data: any): Promise<any> => {
  try {
    Logger.api("actionResetPassword");
    const response = await axiosInstance.post(
      AuthenticationSystem.reset,
      data,
      authHeader
    );

    return response.data;
  } catch (error: any) {
    return extractError(error);
  }
};

export const getCurrentUserInfo = async (): Promise<any> => {
  try {
    Logger.api("getCurrentUserInfo");
    const authUser = await getAuthUser();
    const response = await axiosInstance.get(
      AuthenticationSystem.user + "/" + authUser.id,
      await doAuthorization()
    );

    return response.data;
  } catch (error: any) {
    return extractError(error);
  }
};
