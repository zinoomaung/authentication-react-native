import { axiosInstance, doAuthorization, extractError } from "./Api";
import { getAuthUser } from "../utils/Storage";
import { PokemonSystem } from "./Routes";
import Logger from "../utils/Logger";

export const actionFavouriteCard = async (data: any): Promise<any> => {
  try {
    Logger.api("actionFavouriteCard");
    const authUser = await getAuthUser();
    const response = await axiosInstance.post(
      PokemonSystem.favourite,
      { ...data, userId: authUser.id },
      await doAuthorization()
    );

    return response.data;
  } catch (error: any) {
    return extractError(error);
  }
};

export const getFavouriteCardByUser = async (): Promise<any> => {
  try {
    Logger.api("getFavouriteCardByUser");
    const authUser = await getAuthUser();
    const response = await axiosInstance.get(
      PokemonSystem.favourite + "/" + authUser.id,
      await doAuthorization()
    );

    const favorites = response.data.data;
    return favorites ? favorites.map((favourite: any) => favourite.cardId) : [];
  } catch (error: any) {
    return [];
  }
};
