export const AuthenticationSystem = {
  forgot: "forgot",
  login: "login",
  register: "register",
  reset: "reset",
  user: "user",
};

export const PokemonSystem = {
  favourite: "favourite-card",
};
