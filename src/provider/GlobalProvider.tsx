import React, { createContext, useContext, useState } from "react";

export const GlobalContext = createContext({});

export const GlobalProvider = ({ children }: any) => {
  const [messageCount, setMessageCount] = useState(0);

  const initial = { messageCount, setMessageCount };

  return (
    <GlobalContext.Provider value={initial}>
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobalValue = () => useContext(GlobalContext);
